/**
 * Patrick Miners
 * COP3503
 * Recitation 6
 */


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;


public class Trains {
	public class Train {
		/*
		 *  Simple class to represent an individual train
		 *  running from one station to another.
		 */
		
		int departTime;
		int arriveTime;
		
		public Train(int d, int a) {
			departTime = d;
			arriveTime = a;
		}
		
		public void print() {
			System.out.printf("%d %d\n", departTime, arriveTime);
		}
	}
	
	public class Station {
		/*
		 * Class representing a station. Each station has a list of
		 * trains that depart from it.
		 */
		
		ArrayList<Train> trains;
		
		public Station() {
			trains = new ArrayList<Train>();
		}
		
		public void addTrain(int d, int a) {
			trains.add(new Train(d,a));
		}
		
		public int getBestTrain(int time) {
			/*
			 * Returns -1 if no train can reach the next station
			 * due to time conflicts. Otherwise, returns the train that 
			 * will arrive the soonest at the next station.
			 * 
			 * Runtime is theta(n), where n is the number of trains leaving
			 * the station.
			 */
			
			int arrive = -1;
			
			for(Train t: trains) {
				if (arrive == -1 && time <= t.departTime) {
					arrive = t.arriveTime;
				} else if (time <= t.departTime && t.arriveTime < arrive){
					arrive = t.arriveTime;
				}
			}
	
			return arrive;
		}
		
		public void print() {
			for (Train t: trains) {
				t.print();
			}
		}
	}
	
	ArrayList<Station> stations;
	private final int NUM_STATIONS = 10;
	private boolean canComplete;
	private int penaltyPoints;
	private int finalArriveTime;
	private int n;
	
	public Trains(Scanner s, int n) {
		stations = new ArrayList<Station>();
		
		penaltyPoints = 0;
		canComplete = false;
		this.n = n;
		
		makeTrains(s);
		canReach();
	}
	
	private void makeTrains(Scanner s) {
		/*
		 * Using the scanner, makes all 10 stations and associates the
		 * appropriate trains with it.
		 */
		
		for (int i = 0; i < NUM_STATIONS; i++) {
			int n = s.nextInt();
			
			Station station = new Station();
			
			for (int j = 0; j < n; j++) {
				int a, d;
				d = s.nextInt();
				a = s.nextInt();
				station.addTrain(d, a);
			}
			
			stations.add(station);
		}
	}
	
	private void printStations() {
		int i = 0;
		for (Station s: stations) {
			System.out.printf("Station: %d\n", i);
			s.print();
			i++;
		}
	}
	
	private void print() {
		if (canComplete) {
			System.out.printf("Train %d: Arrive at station #10 in %d minutes with %d penalty points.\n",
					n ,finalArriveTime, penaltyPoints);
		} else {
			System.out.println("Station #10 is NOT reachable.\n");
		}
		
	}
	
	public void canReach() {
		/*
		 * This method iterates through each station, using getBestTrain() for each
		 * station to find the train that will arrive at the next station the quickest.
		 * 
		 * Runtime, worst case with regards to m stations and each station having n trains
		 * is O(mn).
		 */
		int curTime = 0;
		
		for (Station s: stations) {
			penaltyPoints += curTime;
			int arriveTime = s.getBestTrain(curTime);
			
			if (arriveTime == -1) {
				return;
			}
			
			curTime = arriveTime;
			
		}
		
		finalArriveTime = curTime;
		canComplete = true;
		
	}
	
	public static void main(String[] args) {
		Scanner input = null;
		
		try {
			input = new Scanner(new BufferedReader(new FileReader("trains.in")));
			
			int numSchedules = input.nextInt();
			for (int i = 0; i < numSchedules; i++) {
				Trains trains = new Trains(input, i+1);
				
//				trains.printStations();
				trains.print();
				
				if (i+1 < numSchedules) System.out.println("");
			}
			
		} catch (IOException e) {
			System.err.println("Input file not valid:");
			System.err.println(e);
		} finally {
			input.close();
		}

	}
}
