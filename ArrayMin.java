//
// Patrick Miners
// COP 3503
// June 13, 2013
//

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


public class ArrayMin {
	
	private int[] array;
	private int min;
	
	public ArrayMin(int[] n) {
		array = n;
		min = minVal(array);
	}
	
	public static int minVal(int[] numbers) {

		/*
			This will run in O(logn) time, each call splits the array in half
			and makes 2 recursive calls.
		*/
		
		int minLeft, minRight;
		int length = numbers.length;
		int mid = length/2;
		
		// Base case, array of one element
		if (length == 1) {
			return numbers[0];
		}
		int[] leftTemp = new int[mid], rightTemp = new int[length-mid];
		
		// creating new arrays, half the size of the original, 
		System.arraycopy(numbers, 0, leftTemp, 0, mid);
		System.arraycopy(numbers, mid, rightTemp, 0, length-mid);
		
		// finding the minimum of each half
		minLeft = minVal(leftTemp);
		minRight = minVal(rightTemp);
		
		// return the smaller of the two
		if (minLeft < minRight) {
			return minLeft;
		} else {
			return minRight;
		}
	}
	
	public int getMin() {
		return min;
	}
	
	public static void main(String[] args) {
		Scanner input = null;
		
		try {
			input = new Scanner(new BufferedReader(new FileReader("arraymin.in")));
			
			while (input.hasNext()) {
				String buffer = input.nextLine();
				String[] array = buffer.split(" ");
				
				int[] nums = new int[array.length];
				
				for (int i = 0; i < array.length; i++) {
					nums[i] = Integer.parseInt(array[i]);
				}
				
				ArrayMin min = new ArrayMin(nums);
				
				System.out.println(min.getMin());
			}
		} catch (IOException e) {
			System.err.println("Input file not valid:");
			System.err.println(e);
		} finally {
			input.close();
		}
	}

}
