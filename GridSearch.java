//
// Patrick Miners
// COP 3503
// June 13, 2013
//


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


public class GridSearch {
	
	static int[][] board;
	static boolean[][] visited;
	
	int startX, startY, endX, endY;
	boolean pathAvailable;
	
	public GridSearch(int[][] g, int x, int y, int xEnd, int yEnd, int n) {
		board = g;
		visited = new boolean[n][n];
		
		// initize each one to false

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				visited[j][i] = false;
			}
		}
		
		startX = x;
		startY = y;
		endX = xEnd;
		endY = yEnd;
		
		pathAvailable = canMove(startX, startY, endX, endY);
	}
	
	public static boolean canMove(int startx, int starty, int endx,int endy) {
		/*
			Worst case, this is dependent on the board, so the absolute worst case
			it could run on O(n^2) time if it checked every single position on the grid.
		*/
		
		int curNum = board[startx][starty];
		
		visited[startx][starty] = true;
		
		// We've reached the destination
		if ((startx == endx) && (starty == endy)) {
			return true;
		}
		
		// Checking that the next direction is inbounds, the next number is equal or larger, and it hasn't been visited.

		// move right
		if (startx+1 < board.length && curNum <= board[startx+1][starty] && !visited[startx+1][starty]) {
			if (canMove(startx+1, starty, endx, endy)) {
				return true;
			}
		}

		// move down
		if (starty+1 < board.length && curNum <= board[startx][starty+1] && !visited[startx][starty+1]) {
			if (canMove(startx, starty+1, endx, endy)) {
				return true;
			}
		}

		// move left
		if (startx-1 >= 0 && curNum <= board[startx-1][starty] && !visited[startx-1][starty]) {
			if (canMove(startx-1, starty, endx, endy)) {
				return true;
			}
		}

		// move up
		if (starty-1 >= 0 && curNum <= board[startx][starty-1] && !visited[startx][starty-1]) {
			if (canMove(startx, starty-1, endx, endy)) {
				return true;
			}
		}
		
		return false;	
	}
	
	public boolean hasPath() {
		return pathAvailable;
	}
	
	public static void main(String[] args) {
		Scanner input = null;
		
		try {
			input = new Scanner(new BufferedReader(new FileReader("gridsearch.in")));
			
			while (input.hasNext()) {
				int stX, stY, enX, enY, n;
				
				stX = input.nextInt();
				stY = input.nextInt();
				enX = input.nextInt();
				enY = input.nextInt();
				n = input.nextInt();
				
				int[][] gridTemp = new int[n][n];
				
				for (int i = 0; i < n; i++) {
					for (int j = 0; j < n; j++) {
						gridTemp[j][i] = input.nextInt();
					}
				}
				
				GridSearch gSearch = new GridSearch(gridTemp, stX, stY, enX, enY, n);
				System.out.println(gSearch.hasPath());
			}
		} catch (IOException e) {
			System.err.println("Input file not valid:");
			System.err.println(e);
		} finally {
			input.close();
		}
	}
}
