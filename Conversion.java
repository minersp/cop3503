/**
 *  Patrick Miners
 *  COP 3503
 *  June 20, 2013
 *  
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


public class Conversion {
	
	int[][] matrix;
	int[][] list;
	int type;
	int n;
	
	public Conversion(int[][] a, int t, int n) {
		if (t == 1) {
			/**
			 * Matrix -> List runs in O(n^2) time, as worst case each node will be connected to all other
			 * nodes and it's self, needing to do n^2 copies.
			 */
			
			matrix = a;
			list = new int[n][];
			
			// Iterate through each row
			for (int i = 0; i < n; i++) {
				int m = 0;
				// count the number of adjacencies
				for (int j = 0; j < n; j++) {
					if (matrix[i][j] == 1) {
						m++;
					}
				}
				// initialize the array to adjacencies + 1
				list[i] = new int[m+1];
				list[i][0] = m;
				int k = 1;
				// add the adjacencies to the row
				for (int j = 0; j < n; j++) {
					if (matrix[i][j] == 1) {
						list[i][k] = j;
						k++;
					}
				}
			}
			
		} else {
			/**
			 *  List -> runs in O(n^2) time, as worst case each node will be connected to all other
			 * nodes and it's self, needing to do n^2 copies.
			 */
			
			list = a;
			matrix = new int[n][n];
			
			// set everything to 0, just to be sure it's initialized
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					matrix[i][j] = 0;
				}
			}
			
			
			for (int i = 0; i < n; i++) {
				int m = list[i][0];
				
				// add
				for (int j = 1; j < m+1; j++) {
					matrix[i][list[i][j]] = 1;
				}
				
			}
			
		}
		type = t;
		this.n = n;
	}	
	
	public void print() {
		System.out.println(n);
		
		if (type == 1) {
			
			for (int i = 0; i < n; i++) {
				System.out.printf("%d", list[i][0]);
				for (int j = 1; j < list[i][0] + 1; j++){
					System.out.printf(" %d", list[i][j]);
				}
				System.out.println("");
			}
		} else {
			
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					System.out.printf("%d ", matrix[i][j]);
				}
				System.out.println("");
			}
		}
	}
	
	public static void main(String[] args) {
		Scanner input = null;
		
		try {
			input = new Scanner(new BufferedReader(new FileReader("conversions.txt")));
			
			while (true) {
				// 1 is matrix to list. w is list to matrix
				// 0 is end of file
				int t = input.nextInt();
				int n = 0;
				Conversion conv;
				int[][] array = null;
				
				if (t == 0) {
					break;
				}
				
				else if (t == 1) {
					n = input.nextInt();
					
					array = new int[n][n];
					
					for (int i = 0; i < n; i++) {
						for (int j = 0; j < n; j++) {
							array[i][j] = input.nextInt();
						}
					}
				} 
				
				else if (t == 2) {
					n = input.nextInt();
					
					array = new int[n][];
					
					for (int i = 0; i < n; i++) {
						int m = input.nextInt();
						array[i] = new int[m+1];
						array[i][0] = m;
						for (int j = 1; j <= m; j++) {
							array[i][j] = input.nextInt();
						}
					}
				}
				
				conv = new Conversion(array, t, n);
				conv.print();
			}
			
		} catch (IOException e) {
			System.err.println("Input file not valid:");
			System.err.println(e);
		} finally {
			input.close();
		}

	}

}
