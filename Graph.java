import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;


public class Graph {
	public class Node implements Comparable<Node> {
		private int id;
		private ArrayList<Node> adj;
		
		public Node(int id) {
			adj = new ArrayList<Node>();
			this.id = id;
		}
		
		public Node(int id, ArrayList<Node> adj) {
			this.adj = adj;
			this.id = id;
		}
		
		public int getId() {
			return id;
		}
		
		public void addAdj(Node n) {
			adj.add(n);
		}
		
		public ArrayList<Node> getAdjList() {
			return adj;
		}
		
		public boolean isAdj(Node n) {
			return adj.contains(n);
		}

		@Override
		public int compareTo(Node n) {
			if (n.getId() > this.id) {
				return -1;
			} else if (n.getId() < this.id) {
				return 1;
			} else {
				return 0;
			}
		}	
	}
	
	public class Edge {
		Node a;
		Node b;
		
		public Edge(Node a, Node b) {
			this.a = a;
			this.b = b;
		}
	}
	
	ArrayList<Node> nodes;
	ArrayList<Edge> edges;
	int vertices;
	
	public Graph(int v) {
		this.vertices = v;
		
		nodes = new ArrayList<Node>();
		edges = new ArrayList<Edge>();
		for (int i = 0; i < v; i++) {
			nodes.add(new Node(i));
		}
	}
	
	public void addNode(Node n) {
		nodes.add(n);
	}
	
	public void addEdge(int a, int b) {
		Node tempA = nodes.get(a);
		Node tempB = nodes.get(b);
		tempA.addAdj(tempB);
		tempB.addAdj(tempA);
		
		edges.add(new Edge(tempA, tempB));
	}
	
	public void breadthFirstSearch(int n) {
		
		boolean visited[] = new boolean[vertices];
		Queue<Node> q = new LinkedList<Node>();

		q.add(nodes.get(n));
		
		while (!q.isEmpty()) {
			Node curNode = q.remove();
			
			for (Node node: curNode.getAdjList()) {
				if (!visited[node.getId()] && !q.contains(node)) {
					q.add(node);
				}
			}
			
			visited[curNode.getId()] = true;
			System.out.println(curNode.getId());
		}
	}
}
