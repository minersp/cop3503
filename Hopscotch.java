/**
 * Hopscotch.java
 * Patrick Miners
 * Recitation 7
 * COP 3503
 * 
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;


public class Hopscotch {
	
	// HashMap is used to store the scores for a value(key)
	static HashMap<Integer, Integer> scores;
	
	// This simply tracks the highest value we have computed the score for.
	static int highest;
	
	public Hopscotch() {
		scores = new HashMap<Integer, Integer>();
		
		// Initialize 1 to the hashmap
		highest = 1;
		scores.put(1, 1);
	}
	
	public int getScore(int n) {
		if (n > highest) {
			updateMap(n);
		}	
		return scores.get(n);
		
	}
	
	private void updateMap(int n) {
		
		/*
		 * Iterative implementation. It updates from highest + 1 to the current n.
		 */
		
		for (int temp = highest + 1; temp <= n; temp++) {
			
			// use this as the base and check everything against it.
			int bestScore = 1 + scores.get(temp-1);
			
			if (isPrime(temp) && temp > 10) {
				int skip = temp % 10;
				int testScore = 3 + scores.get(temp-skip);
				
				if (testScore < bestScore) {
					bestScore = testScore;
				}
			}
			
			if (temp % 7 == 0) {
				int testScore = 2 + scores.get(temp - 4);
				
				if (testScore < bestScore) {
					bestScore = testScore;
				}
			}
			
			if (temp % 11 == 0) {
				int skip = sumDigits(temp);
				
				int testScore = 4 + scores.get(temp - skip);
				
				if (testScore < bestScore) {
					bestScore = testScore;
				}
			}
			
			scores.put(temp,  bestScore);
			highest = temp;
		}
	}
	
	private void updateMapRec(int n) {
		/*
		 * Recursive implementation
		 * recursively updates from highest + 1 to the current n the score for each value
		 */
		
		if (n == highest) {
			return;
		}
		
		// First we need to update the values lower than this.
		updateMapRec(n-1);
		
		// use this as the base and check everything against it.
		int bestScore = scores.get(n-1) + 1;
		
		if (isPrime(n) && n > 10) {
			int skip = n % 10;
			int testScore = 3 + scores.get(n-skip);
			
			if (testScore < bestScore) {
				bestScore = testScore;
			}
		}
		
		if (n % 7 == 0) {
			int testScore = 2 + scores.get(n-4);
			
			if (testScore < bestScore) {
				bestScore = testScore;
			}
		}
		
		if (n % 11 == 0) {
			int skip = sumDigits(n);
			
			int testScore = 4 + scores.get(n - skip);
			
			if (testScore < bestScore) {
				bestScore = testScore;
			}
		}
		
		scores.put(n, bestScore);
		highest = n;
	}

	private int sumDigits(int n) {
		int t = n;
		int sum = 0;
		
		// This simply mods the last digit and divides by 10 to shift
		// the number over one digit.
		while (t > 0) {
			sum += t % 10;
			t = t/10;
		}	
		return sum;
	}

	private boolean isPrime(int n) {
		if (n < 11) {
			return false;
		}
		
		// Slow implementation
		for (int i = 2; i <= n/2; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}
	
	private void printHash() {
		for (int i = 1; i <= highest; i++) {
			System.out.println(i + " " + scores.get(i));
		}
	}

	public static void main(String[] args) {
		Scanner input = null;
		
		try {
			input = new Scanner(new BufferedReader(new FileReader("hopscotch.in")));
			
			Hopscotch test = new Hopscotch();
			
			int n = input.nextInt();
			
			for (int i = 1; i <= n; i++) {
				int num = input.nextInt();
				System.out.printf("Game #%d: %d\n", i, test.getScore(num));
			}
			
		} catch (IOException e) {
			System.err.println("Input file not valid:");
			System.err.println(e);
		} finally {
			input.close();
		}

	}
}

/**
 * scores is a data structure that supports adding dynamically and associating a key to a score.
 * 
 * 
 * getScore(n) {
 *     if (n > scores.high)
 *         updateScores(n)
 *     return scores.get(n)
 *     
 * }
 * 
 * updateScores(n) {
 *     for i from scores.high + 1 to n {
 *         bestScore = 1 + scores.get(i)
 *         if (isPrime(i) and i > 10) {
 *             tempScore = 3 + scores.get(i - (i%10));
 *             if (tempScore < bestScore)
 *                 bestScore = tempScore
 *         }
 *         
 *         if (i % 7 = 0) {
 *             tempScore = 2 + scores.get(i-4)
 *             if (tempScore < bestScore)
 *                 bestScore = tempScore
 *         }
 *         
 *         if (i % 11 = 0) {
 *             skip = sumDigits(i);
 *             tempScore = 4 + scores.get(i-skip)
 *             if (tempScore < bestScore)
 *                 bestScore = tempScore
 *         }
 *         
 *         scores.add(i, bestScore)
 *     }
 * }
 * 
 */

