/**
 * Patrick Miners
 * COP 3503c
 * Recitation 8
 * 
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


public class Maze {

	int[][] maze;
	boolean[][] visited;
	int size;
	boolean pathAvailable;
	
	public Maze(Scanner in, int n) {
		maze = new int[n][n];
		visited = new boolean[n][n];
		size = n;

		buildMaze(in);
		
		// start at x = 0, y = 1.
		pathAvailable = findPathAvailable(0,1);
	}
	
	public void print() {
		if (pathAvailable) {
			System.out.println("The robot CAN get to the other side");
		} else {
			System.out.println("The robot CAN'T get to the other side");
		}
	}
	
	private boolean findPathAvailable(int x, int y) {
		
		visited[x][y] = true;

		// if we're at the furthest right column
		if (x == size-1) {
			return true;
		}
		
		// Move right
		if (x+1 < size && maze[x+1][y] == 0 && !visited[x+1][y]) {
			if (findPathAvailable(x+1,y)) {
				return true;
			}
		}
		
		// Move down
		if (y+1 < size && maze[x][y+1] == 0 && !visited[x][y+1]) {
			if (findPathAvailable(x,y+1)) {
				return true;
			}
		}
		
		// Move up
		if (y-1 >= 0 && maze[x][y-1] == 0 && !visited[x][y-1]) {
			if (findPathAvailable(x, y-1)) {
				return true;
			}
		}
		
		// Move left
		if (x-1 >= 0 && maze[x-1][y] == 0 && !visited[x-1][y]) {
			if (findPathAvailable(x-1,y)) {
				return true;
			}
		}

		return false;		
	}

	void buildMaze(Scanner in) {
		for(int i = 0; i < size; i++) {
			String l = in.next();
			
			for (int ii = 0; ii < size; ii++) {
				if (l.charAt(ii) == 'x') {
					maze[ii][i] = 1;
				} else {
					maze[ii][i] = 0;
				}
			}
		}
	}
	
	public static void main(String[] args) {
		Scanner input = null;
		
		try {
			input = new Scanner(new BufferedReader(new FileReader("maze.in")));
			
			int n = input.nextInt();

			for (int i = 0; i < n; i ++) {
				int m = input.nextInt();
				Maze maze = new Maze(input, m);
				maze.print();
			}	
			
		} catch (IOException e) {
			System.err.println("Input file not valid:");
			System.err.println(e);
		} finally {
			input.close();
		}
	}
} 
