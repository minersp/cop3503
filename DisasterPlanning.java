/**
 *  Author: Patrick Miners
 *  COP 3503c
 *  Recitation 5
 *  6/27/13
 */


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;


public class DisasterPlanning {

	public class Graph {
		
		/*
		 * Class implementation of an adjacency list.
		 * Also has a list of edges to preserve the order
		 * from the input file and to use to iterate
		 * through each edge when testing for critical 
		 * edges
		 */
		
		public class Node implements Comparable<Node> {
			private int id;
			private ArrayList<Node> adj;
			
			public Node(int id) {
				adj = new ArrayList<Node>();
				this.id = id;
			}
			
			public Node(int id, ArrayList<Node> adj) {
				this.adj = adj;
				this.id = id;
			}
			
			public int getId() {
				return id;
			}
			
			public void addAdj(Node n) {
				adj.add(n);
			}
			
			public void removeAdj(Node n) {
				adj.remove(n);
			}
			
			public ArrayList<Node> getAdjList() {
				return adj;
			}
			
			public boolean isAdj(Node n) {
				return adj.contains(n);
			}

			@Override
			public int compareTo(Node n) {
				if (n.getId() > this.id) {
					return 1;
				} else if (n.getId() < this.id) {
					return -1;
				} else {
					return 0;
				}
			}	
		}
		
		public class Edge {
			Node a;
			Node b;
			
			public Edge(Node a, Node b) {
				this.a = a;
				this.b = b;
			}

			public void print() {
				System.out.printf("%d %d\n", a.id, b.id);
				
			}
		}
		
		ArrayList<Node> nodes;
		ArrayList<Edge> edges;
		ArrayList<Edge> critEdges;
		int vertices;
		
		public Graph(int v) {
			this.vertices = v;
			
			nodes = new ArrayList<Node>();
			edges = new ArrayList<Edge>();
			critEdges = new ArrayList<Edge>();
			
			// As all nodes are numbered from 0 to n
			// there is no reason we can't implicitly
			// initialize them
			for (int i = 0; i < v; i++) {
				nodes.add(new Node(i));
			}
		}
		
		public void addNode(Node n) {
			nodes.add(n);
		}
		
		public void addEdge(int a, int b) {
			// First add each edge to both nodes' adj list
			Node tempA = nodes.get(a);
			Node tempB = nodes.get(b);
			tempA.addAdj(tempB);
			tempB.addAdj(tempA);
			
			// Next add it to the master list of edges
			edges.add(new Edge(tempA, tempB));
		}
		
		public void addEdge(Node a, Node b) {
			/*
			 * This particular method is used during critical edge testing
			 * to place an edge back into the adj. list of the corresponding
			 * nodes after it has been tested as a critical edge. There is no
			 * need to add it back to the master list as it never gets removed
			 * there
			 */
			
			a.addAdj(b);
			b.addAdj(a);
		}
		
		public void removeEdge(Node a, Node b) {
			/*
			 * Same story as addEdge. This is used during testing to remove an edge
			 * at the start of each test.
			 */
			
			a.removeAdj(b);
			b.removeAdj(a);
		}
		
		private boolean[] bfs(int n) {
			/*
			 * Simple breadth first search implementation. Each node is marked visited
			 * on an array to be checked after the search is complete for completeness.
			 */
			
			boolean visited[] = new boolean[vertices];
			Queue<Node> q = new LinkedList<Node>();

			q.add(nodes.get(n));
			
			while (!q.isEmpty()) {
				Node curNode = q.remove();
				
				for (Node node: curNode.getAdjList()) {
					if (!visited[node.getId()] && !q.contains(node)) {
						q.add(node);
					}
				}
				
				visited[curNode.getId()] = true;
			}
			
			return visited;
		}
		
		private boolean isConnected() {
			/*
			 * Initiates a bfs of the graph and the checks if all nodes were visited.
			 * If a node was not visited, the graph is not complete.
			 */
			
			boolean visited[] = bfs(0);
			
			for (int i = 0; i < vertices; i++) {
				if (!visited[i]) {
					return false;
				}
			}
			
			return true;
		}
		
		public void findCritEdges() {
			/*
			 * Iterates through each edge, removing it from the graph to test
			 * if it remains connected after that edge is removed. If it does not,
			 * that edge is then added to the critical edge list.
			 */
			
			/**
			 * For the number of nodes n, and the number of edges m. As his implementation
			 * iterates through m edges, and during each iteration visits at most n nodes,
			 * the algorithm will check for critical edges in O(n*m) time.
			 */
			
			
			for (Edge e: edges) {
				Node tempA = e.a;
				Node tempB = e.b;
				
				removeEdge(tempA, tempB);
				boolean isCritEdge = !isConnected();
				
				if (isCritEdge) {
					critEdges.add(e);
				}
				
				addEdge(tempA, tempB);
			}	
		}

		public void printCritEdges() {
			/*
			 * Prints each edge from critEdges.
			 */
			
			if (critEdges.isEmpty()) {
				System.out.println("No critical edges found");
			} else {
				for (Edge e: critEdges) {
					e.print();
				}
			}
			
		}
	}
	
	public DisasterPlanning() {
		Scanner input = null;
		
		try {
			input = new Scanner(new BufferedReader(new FileReader("connectivity.txt")));
			
			int v = input.nextInt();
			int e = input.nextInt();
			
			Graph g = new Graph(v);
			
			for (int i = 0; i < e; i++) {
				int a = input.nextInt();
				int b = input.nextInt();
				
				g.addEdge(a, b);
			}
			
			g.findCritEdges();
			g.printCritEdges();
			
		}  catch (IOException e) {
			System.err.println("Input file not valid:");
			System.err.println(e);
		} finally {
			input.close();
		}
	}

	public static void main(String[] args) {
		new DisasterPlanning();
	}
}
