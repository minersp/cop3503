//
// Patrick Miners
// COP 3503
// June 13, 2013
//

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


public class NumOnes {

	private int number;
	private int numOnes;
	
	public NumOnes(int n) {
		number = n;
		numOnes = findNumOnes(number);
	}
	
	private int findNumOnes(int n) {
		/*
			This will run in O(logn) time, because a number can have at most
			log base 2 1's in it's binary representaion.
		*/
		
		// if the remainder is 0
		if(n < 1) {
			return 0;
		}
		
		// find what is the largest power of 2 that is smaller than n
		int pow = (int) (Math.log(n)/Math.log(2));

		// return 1 + the remainder, because the number must be greater than 1, so there will
		// be at least one 1.
		// then a recursive call on the remainder
		return 1 + findNumOnes(n - (int) Math.pow(2, pow));
	}
	
	public int getNumOnes() {
		return numOnes;
	}
	
	public static void main(String[] args) {
		Scanner input = null;
		
		try {
			input = new Scanner(new BufferedReader(new FileReader("numones.in")));
			
			while (input.hasNextInt()) {
				NumOnes numones = new NumOnes(input.nextInt());
				
				System.out.println(numones.getNumOnes());
			}
			
		} catch (IOException e) {
			System.err.println("Input file not valid:");
			System.err.println(e);
		} finally {
			input.close();
		}

	}

}
